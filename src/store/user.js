import axios from 'axios'
import Vue from 'vue'

export default {
    namespaced: true,

    state: () => ({
        token: '',
        logoutTimeout: 0,
        id: '',
        name: '',
        phone: '',
        email: '',
        photo: '',
        permissions: {},
        ...JSON.parse(localStorage.getItem('memorizedUser') || '{}')
    }),
    getters: {
        hasPermissions: (state) => (permissions, every = false) => {
            let method = every ? 'every' : 'some';
    
            return permissions[method](permission => !!state.permissions[permission])
        }
    },
    mutations: {
        settoken(state, token) {
            state.token = token
        },
        setUserData(state, data) {
            localStorage.setItem('memorizedUser', JSON.stringify(data));
            
            state.id = data.id;
            state.name = data.name;
            state.phone = data.phone;
            state.email = data.email;
            state.photo = data.photo;
            state.token = data.token;
    
            (data.permissions || []).forEach(permission => {
                Vue.set(state.permissions, permission, true)
            });
        },
        updatePermissions(state, permissions) {
            state.permissions = {};
            (permissions || []).forEach(permission => {
                Vue.set(state.permissions, permission, true)
            });
        }
    },
    actions: {
        async onAuthStateChanged({ state, commit, dispatch }, authUser) {
            if (authUser) {
                // if (!state.token) {
                let token = await cordova.plugins.firebase.auth.getIdToken()
                let response = await axios.post('https://dev.ultrashop.uz/api/firebase_login', { firebasetoken: token });

                if (response.data.permissions && response.data.permissions.length) {
                    commit('setUserData', response.data)
                }
                // }
            } else {
                if (state.token) {
                    let token = state.token;
                    commit('setUserData', {})
        
                    await axios.post('https://dev.ultrashop.uz/api/firebase_logout', {}, {
                        headers: {
                            Authorization: `Bearer ${token}`
                        }
                    })
                }
            }
        },
    }
}