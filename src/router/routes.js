
const routes = [
  {
    path: '/',
    name: 'Auth',
    component: () => import('pages/auth.vue')
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('pages/dashboard')
      }
    ]
  }
]

export default routes
