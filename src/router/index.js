import Vue from 'vue'
import VueRouter from 'vue-router'
import VueTheMask from 'vue-the-mask'
import VueBarcodeScanner from 'src/plugins/barcode'

import routes from './routes'
import store from "src/store/store";

(function(ELEMENT) {
  ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
  ELEMENT.closest = ELEMENT.closest || function closest(selector) {
      if (!this) return null;
      if (this.matches(selector)) return this;
      if (!this.parentElement) {return null}
      else return this.parentElement.closest(selector)
    };
}(Element.prototype));

Vue.use(VueRouter)
Vue.use(VueTheMask)
Vue.use(VueBarcodeScanner)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */
const Router = new VueRouter({
  scrollBehavior: () => ({ x: 0, y: 0 }),
  routes,

  // Leave these as they are and change in quasar.conf.js instead!
  // quasar.conf.js -> build -> vueRouterMode
  // quasar.conf.js -> build -> publicPath
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE
})

Router.beforeEach((to, from, next) => {
  if (to.name !== 'Auth' && !store.state.user.token) next({name: 'Auth'})
  else if (to.name === 'Auth' && store.state.user.token) next({name: 'Dashboard'})
  else next()
})

document.addEventListener("deviceready", () => {
  console.log(cordova.plugins)
  cordova.plugins.firebase.auth.onAuthStateChanged(user =>  {
    store.dispatch('user/onAuthStateChanged', user).then(() => {
      if(store.state.user.token && Router.currentRoute.name === 'Auth') Router.push({name: 'Dashboard'})
      else if (!store.state.user.token && Router.currentRoute.name !== 'Auth') Router.push({name: 'Auth'})
    })
  });
}, false);


export default Router
//  (/* { store, ssrContext } */) {


//   return Router
// }
