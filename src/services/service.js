import axios from 'axios'
import axiosRetry from 'axios-retry'
import store from 'src/store/store'
import router from 'src/router/index'

export class Service {
	constructor() {
        this.instance = axios.create({
            baseURL: 'https://dev.ultrashop.uz/api'
        })

        this.instance.interceptors.request.use((request) => {
            if (store.state.user.token)
                request.headers.Authorization = 'Bearer ' + store.state.user.token
            return request
        })

        this.instance.interceptors.response.use((response) => {
            return response
        }, error => {
            if (error.response && error.response.status == 401) {
                store
                    .dispatch('user/onAuthStateChanged', null)
                    .finally(() => {
                        router.push('/')
                    })
            }
            return Promise.reject(error)
        })

        axiosRetry(this.instance, { retries: 3, retryDelay: axiosRetry.exponentialDelay });
	}
}