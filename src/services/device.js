import { Service } from './service'

class Product extends Service {
	constructor() {
		super();
	}

	register(token) {
		return this.instance.post(`/register_fcm`, { token }).then(response => {
			return response.data;
		})
	}
}

export default new Product()