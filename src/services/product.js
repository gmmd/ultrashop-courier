import { Service } from './service'

class Product extends Service {
	constructor() {
		super();
	}

	get(id) {
		return this.instance.get(`/products/${id}`).then(response => {
			return response.data;
		})
	}
}

export default new Product()