import { Service } from './service'

class StockMovements extends Service {
	constructor() {
		super();
	}

	create(data) {
		return this.instance.post(`/warehouses/stock_movements`, data).then(response => {
			return response.data
		})
	}

	update(data) {
		return this.instance.put(`/warehouses/stock_movements`, data).then(response => {
			return response.data
		})
	}

	get(id) {
		return this.instance.get(`/warehouses/stock_movements/${id}`).then(response => {
			return response.data
		})
	}

	getAll(params = {}) {
		return this.instance.get(`/warehouses/stock_movements`, { params }).then(response => {
			return response.data
		})
	}
}

export default new StockMovements()